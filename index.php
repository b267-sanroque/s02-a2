<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP S02 Activity</title>
  </head>
  <body>
  	<h1>Divisible of Five</h1>

  	<p><?php printDivisibleOfFive(); ?></p>

  	<h1>Array Manipulation</h1>

  	<?php array_push($students, "Jane Doe"); ?>
  	<pre><?php print_r($students); ?></pre>

  	<p><?php echo count($students); ?></p>

  	<?php array_push($students, "John Smith"); ?>
  	<pre><?php print_r($students); ?></pre>
  	<p><?php echo count($students); ?></p>

  	 <?php array_shift($students); ?>
  	 <pre><?php print_r($students); ?></pre>
  	<p><?php echo count($students); ?></p>

  </body>
</html>